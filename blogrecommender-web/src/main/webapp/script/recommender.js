function getBlogs() {
    $.getJSON("rest/recommender/blogs",
             function(data) {
                 if (data != null) {
                      $.each(data.blog, function(i, item) {
                           thisItem = $("<li class='arrow'/>");
                         thisItem.appendTo("#blogs");
                         $("<a/>").attr("href", item.url).text(item.title).appendTo(thisItem);
                      });
                 }
             });
}