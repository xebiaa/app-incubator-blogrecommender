package com.xebia.appinc.blogrecommender.recommender;

import java.util.List;

import com.xebia.appinc.blogrecommender.model.Blog;

/**
 * A silly, temporary implementation of the RecommendationCentre interface to demonstrate
 * guice multi-bindings in action.
 */
public class DummyRecommendationCentre implements IRecommendationCentre {

    public static final String KEY = "dummy";

    @Override
    public List<Blog> getRecommendations(Blog blog) {
        // Vraag mij niet naar mijn mening, zoek liever op Google.
        return java.util.Collections.emptyList();
    }
}
