package com.xebia.appinc.blogrecommender.recommender;

import com.dasberg.guice.InjectDependencies;
import com.google.inject.Inject;
import com.xebia.appinc.blogrecommender.model.Blog;
import com.xebia.appinc.blogrecommender.storage.Storage;

import java.util.List;
import java.util.Random;

/**
 * RecommendationCenter.
 * @author mischa
 */
public class RecommendationCentre implements IRecommendationCentre {

    @Inject
    private Storage storage;

    public static final String KEY = "default";
    private static final int AMOUNT_OF_RECOMMENDATIONS = 3;

    @InjectDependencies
    public RecommendationCentre() {
    }

    @Override
    public List<Blog> getRecommendations(Blog blog) {
        List<Blog> recommendations = storage.getRecommendations(blog.getAuthor());
        return recommendations.size() > AMOUNT_OF_RECOMMENDATIONS ? recommendations.subList(0, AMOUNT_OF_RECOMMENDATIONS) : recommendations;
    }
}
