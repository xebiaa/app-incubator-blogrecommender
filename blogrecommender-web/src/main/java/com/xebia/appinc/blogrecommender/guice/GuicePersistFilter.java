package com.xebia.appinc.blogrecommender.guice;

import com.dasberg.guice.InjectDependencies;
import com.google.inject.Singleton;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.UnitOfWork;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Guice persistence filter.
 * @author mischa
 */
@Singleton
@WebFilter(urlPatterns="/*")
public class GuicePersistFilter implements Filter {

    @Inject
    private UnitOfWork unitOfWork;
    @Inject
    private PersistService persistService;

    /** Default constructor. */
    @InjectDependencies
    public GuicePersistFilter() {
    }

    /** {@inheritDoc}. */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LoggerFactory.getLogger(GuicePersistFilter.class).info("GuicePersistFilter init'ed.");
        persistService.start();
    }

    /** {@inheritDoc}. */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        unitOfWork.begin();
        try {
            chain.doFilter(request, response);
        } finally {
            unitOfWork.end();
        }
    }

    /** {@inheritDoc}. */
    @Override
    public void destroy() {
        persistService.stop();
    }
}
