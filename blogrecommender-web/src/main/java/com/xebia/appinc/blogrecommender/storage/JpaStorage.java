package com.xebia.appinc.blogrecommender.storage;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import com.dasberg.guice.InjectDependencies;
import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import com.xebia.appinc.blogrecommender.model.Blog;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.TermQuery;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;

/**
 * Implements the {@link Storage} interface based on JPA.
 * @author barend
 */
public class JpaStorage implements Storage {

    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    @Inject
    private EntityManager em;

    @InjectDependencies
    public JpaStorage() {
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Blog> getAllBlogs() {
        return em.createNamedQuery("Blog.findAll").getResultList();
    }

    @Override
    @Transactional
    public void addBlog(Blog blog) {
        Blog possiblyAlreadyExistingBlog = findBlog(blog.getUrl());
        if (possiblyAlreadyExistingBlog != null) {
            em.remove(possiblyAlreadyExistingBlog);
            em.flush();
        }
        em.persist(blog);
    }

    @Override
    public Blog findBlog(String blogUrl) {
        try {
            return (Blog) em.createNamedQuery("Blog.findBlog")
                    .setParameter("blogUrl", blogUrl).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }

    }

    @Override
    public List<Blog> getRecommendations(String author) {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);
        TermQuery query = new TermQuery(new Term("author", author));
        List<Blog> blogs = fullTextEntityManager.createFullTextQuery(query, Blog.class).getResultList();
        return blogs;
    }
}
