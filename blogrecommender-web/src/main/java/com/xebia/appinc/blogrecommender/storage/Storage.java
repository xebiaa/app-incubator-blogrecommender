package com.xebia.appinc.blogrecommender.storage;

import java.util.List;

import com.xebia.appinc.blogrecommender.model.Blog;

/**
 * Service provider interface for data storage.
 * 
 * @author barend
 */
public interface Storage {

	void addBlog(Blog blog);

	List<Blog> getAllBlogs();

	Blog findBlog(String blogUrl);

    List<Blog> getRecommendations(String author);
}
