package com.xebia.appinc.blogrecommender.model;

import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.*;
import org.joda.time.DateTime;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

/** @author barend */
@Entity
@Table(name = "blog", uniqueConstraints = @UniqueConstraint(columnNames = "url"))
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlogType")
@XmlRootElement(name = "blog")

@NamedQueries({
        @NamedQuery(name = "Blog.findAll", query = "SELECT OBJECT(b) FROM Blog b "),
        @NamedQuery(name = "Blog.findAllFromAuthor", query = "SELECT OBJECT(b) FROM Blog b WHERE b.author=:author"),
        @NamedQuery(name = "Blog.findBlog", query = "SELECT OBJECT(b) FROM Blog b WHERE b.url=:blogUrl")
})

@Indexed
@Analyzer(impl = org.apache.lucene.analysis.standard.StandardAnalyzer.class)
public class Blog {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @DocumentId
    private int id;

    @Column(name = "url", unique = true)
//    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    private String url;

    @Column(name = "title")
//    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    private String title;

    @Column(name = "author")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    private String author;

    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    @Column(name = "date")
    private DateTime date;

    @CollectionOfElements(targetElement = java.lang.String.class)
    @JoinTable
    private Set<String> tags;

    @Column(name = "text")
    private String text;

    public Blog() {
    }

    /**
     * Constructor.
     * @param title  The title.
     * @param author The author.
     * @param date   The date.
     * @param tags   The tags.
     * @param text   The text.
     */
    public Blog(String url, String title, String author, DateTime date, Set<String> tags, String text) {

        this.url = url;
        this.title = title;
        this.author = author;
        this.date = date;
        this.tags = tags;
        this.text = text;
    }

    public Blog(String shouldGetParsed) {
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public DateTime getDate() {
        return date;
    }

    public Set<String> getTags() {
        return tags;
    }

    public String getText() {
        return text;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return String.format("Blog [id=%s, author=%s, date=%s, url=%s]", id, author, date, url);
    }
}
