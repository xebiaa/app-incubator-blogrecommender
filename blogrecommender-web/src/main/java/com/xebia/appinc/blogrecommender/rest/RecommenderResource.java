package com.xebia.appinc.blogrecommender.rest;

import com.dasberg.guice.InjectDependencies;
import com.xebia.appinc.blogrecommender.model.Blog;
import com.xebia.appinc.blogrecommender.recommender.IRecommendationCentre;
import com.xebia.appinc.blogrecommender.storage.Storage;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.tree.DefaultCDATA;
import org.dom4j.tree.DefaultElement;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.IOException;
import java.util.*;

/**
 * Rest resource for the expense module.
 * @author mischa
 */
@Path("/recommender/")
public class RecommenderResource {
    private static DateTimeFormatter fmt = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss Z").withLocale(Locale.US);
    private final Logger logger = LoggerFactory.getLogger(RecommenderResource.class);

    @Inject
    private Storage storageService;

    @Inject
    private Map<String, IRecommendationCentre> recommendationServices;

    /** Default constructor. */
    @InjectDependencies
    public RecommenderResource() {
    }

    @Path("blogs")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Blog> getAllBlogs() {
        return storageService.getAllBlogs();
    }

    @Path("add/rssfeed")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addRssFeed(@FormParam("feedUrl") String feedUrl) {
        try {
            GetMethod method = new GetMethod(feedUrl);
            int i = new HttpClient().executeMethod(method);
            if (i == 200) {
                Document document = DocumentHelper.parseText(new String(method.getResponseBody()));
                for (DefaultElement element : (List<DefaultElement>) document.selectNodes("//item")) {
                    String title = element.selectSingleNode("title").getText();
                    String link = element.selectSingleNode("link").getText();
                    String pubDate = element.selectSingleNode("pubDate").getText();
                    String author = element.selectSingleNode("dc:creator").getText();
                    String description = element.selectSingleNode("description").getText();
                    Set<String> categories = new HashSet<String>();
                    for (DefaultElement category : (List<DefaultElement>) element.selectNodes("category")) {
                        for(Object c: category.content()) {
                            if (c instanceof DefaultCDATA) {
                                categories.add(((DefaultCDATA) c).getStringValue());
                            }
                        }
                    }
                    if (description.length() > 250) {
                        description = description.substring(0, 250);
                    }
                    storageService.addBlog(new Blog(link, title, author, fmt.parseDateTime(pubDate), categories, description));
                }
            } else {
                return Response.status(400).type("text/plain").entity("Feed not found.").build();
            }
        } catch (IOException e) {
            return Response.status(400).type("text/plain").entity(e.getMessage()).build();
        } catch (DocumentException e) {
            return Response.status(400).type("text/plain").entity(e.getMessage()).build();
        }

        return Response.ok("Feed: " + feedUrl + " was parsed successfully").build();
    }

    @Path("recommend")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Blog> getRecommendation(@QueryParam("blogUrl") String blogUrl, @QueryParam("engine") @DefaultValue("default") String engine) {
        IRecommendationCentre recommender = recommendationServices.get(engine);
        logger.info("Recommending for {} with {}", blogUrl, engine);
        if (recommender == null) {
            logger.info("There is no engine {}, throwing HTTP 400", engine);
            throw new WebApplicationException(Response.status(Status.BAD_REQUEST).build());
        }
        return recommender.getRecommendations(storageService.findBlog(blogUrl));
    }

    @Path("add/testdata")
    @GET
    public Response addTestData() {
        storageService.addBlog(new Blog("http://blog.xebia.com/1", "First Blog", "~lvanderpoel", null, null, "Very smart text"));
        storageService.addBlog(new Blog("http://blog.xebia.com/2", "Second Blog", "~lvanderpoel", null, null, "Very smart text"));
        storageService.addBlog(new Blog("http://blog.xebia.com/3", "Third Blog", "~lvanderpoel", null, null, "Very smart text"));
        storageService.addBlog(new Blog("http://blog.xebia.com/4", "Fourth Blog", "~lvanderpoel", null, null, "Very smart text"));
        storageService.addBlog(new Blog("http://blog.xebia.com/5", "Fifth", "~mdasberg", null, null, "Very smart text"));
        storageService.addBlog(new Blog("http://blog.xebia.com/6", "Sixth Blog", "~mdasberg", null, null, "Very smart text"));
        storageService.addBlog(new Blog("http://blog.xebia.com/7", "Seventh Blog", "~mdasberg", null, null, "Very smart text"));
        storageService.addBlog(new Blog("http://blog.xebia.com/8", "Eighth Blog", "~bgarvelink", null, null, "Very smart text"));
        storageService.addBlog(new Blog("http://blog.xebia.com/9", "Ninth Blog", "~bgarvelink", null, null, "Very smart text"));
        return Response.ok().build();
    }
}
