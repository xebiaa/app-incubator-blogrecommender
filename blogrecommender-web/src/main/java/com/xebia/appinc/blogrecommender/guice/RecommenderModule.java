package com.xebia.appinc.blogrecommender.guice;

import java.io.IOException;
import java.util.Properties;

import com.dasberg.guice.Module;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.google.inject.servlet.ServletModule;
import com.xebia.appinc.blogrecommender.recommender.DummyRecommendationCentre;
import com.xebia.appinc.blogrecommender.recommender.IRecommendationCentre;
import com.xebia.appinc.blogrecommender.recommender.RecommendationCentre;
import com.xebia.appinc.blogrecommender.storage.JpaStorage;
import com.xebia.appinc.blogrecommender.storage.Storage;

/**
 * @author mischa
 */
@Module
public class RecommenderModule extends ServletModule {

    /** {@inheritDoc}. */
    @Override
    protected void configureServlets() {
        JpaPersistModule jpaPersistModule = new JpaPersistModule("x");
        try {
            Properties properties = new Properties();
            properties.load(RecommenderModule.class.getResourceAsStream("/persistence.properties"));
            jpaPersistModule.properties(properties);
        } catch (IOException e) {
            // no properties
        }
        install(jpaPersistModule);
        bind(Storage.class).to(JpaStorage.class);
        
        MapBinder<String, IRecommendationCentre> recommendationCentreBinder = 
                MapBinder.newMapBinder(binder(), String.class, IRecommendationCentre.class);
        
        recommendationCentreBinder.addBinding(DummyRecommendationCentre.KEY).to(DummyRecommendationCentre.class);
        recommendationCentreBinder.addBinding(RecommendationCentre.KEY).to(RecommendationCentre.class);
    }
}
