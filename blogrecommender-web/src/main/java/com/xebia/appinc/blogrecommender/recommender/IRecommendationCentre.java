package com.xebia.appinc.blogrecommender.recommender;

import java.util.List;

import com.xebia.appinc.blogrecommender.model.Blog;

/**
 * Service provider interface for the recommender.
 * 
 * @author barend
 */
public interface IRecommendationCentre {
    
	List<Blog> getRecommendations(Blog blog);
}
